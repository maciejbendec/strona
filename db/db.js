//////////////SEQUELIZE\
var Sequelize = require('sequelize');
var DataTypes = require('sequelize/lib/data-types');
 var sequelize = new Sequelize('baza','', '',
  {
   dialect: 'sqlite',
   storage: '/home/maciej/hello/baza.db',
  }
 );
var User = sequelize.define('users', 
{
  Id : {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
  Name: DataTypes.STRING,
  Surname : DataTypes.STRING,
  Librarian : DataTypes.BOOLEAN,
  Login : DataTypes.STRING,
  Password : DataTypes.STRING
},{
    timestamps: false
})

User.sync();
var Book = sequelize.define('books', 
{
  ISBN: {
        type: DataTypes.STRING,
        primaryKey: true
    },
  Title: DataTypes.STRING,
  AuthorName: DataTypes.STRING,
  Year : DataTypes.INTEGER,
  Description : DataTypes.STRING,
},{
    timestamps: false
})
Book.sync();
var Copy = sequelize.define('copies', 
{
  Id : {
        type: DataTypes.INTEGER,
        primaryKey: true
    },
  BookISBN: DataTypes.STRING,
  UserId : DataTypes.INTEGER,
},{
    timestamps: false
})
Copy.sync();
//////////////RELATIONS
Copy.belongsTo(User);
User.hasMany(Copy);
Copy.belongsTo(Book);
Book.hasMany(Copy);
Book.belongsToMany(User, { "through" : Copy });
//////////////END SEQUELIZE
//////////////////////////PASSPORT
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
// Serialize sessions
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});
// Use local strategy to create user account
passport.use(new LocalStrategy(
  function(Login, Password, done) {
    User.find({ where: { Login: Login }}).then(function(user) {
      if (!user) {
        done(null, false, { message: 'Unknown user' });
      } else if (Password != user.Password) {
        done(null, false, { message: 'Invalid password'});
      } else {
        console.log('Setting user role!!!!!!!!!!!!!!!!!!!!');
        user.role='librarian';
        done(null, user);
      }
    }).error(function(err){
      done(err);
    });
  }
));
////////////////////// END PASSPORT
module.exports.User = User;
module.exports.Copy = Copy;
module.exports.Book = Book;
module.exports.passport = passport;
module.exports.sequelize = sequelize;