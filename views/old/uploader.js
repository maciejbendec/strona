var express = require('express');
var router = express.Router();

router.get('/',function(req,res,next){
      //res.sendFile(__dirname + "/index.html");
      //res.send('respond with a resource');
      res.render('uploader');
});

router.post('/api/photo',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.end("File is uploaded");
    });
});

module.exports = router;