var express = require('express');
var router = express.Router();
var multer  =   require('multer');
var db= require('/home/maciej/hello/db/db.js');
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, '/home/maciej/hello/public/uploads');
  },
	filename: function (req, file, cb) {
			var getFileExt = function(fileName){
					console.log("Costam1");
					console.log(fileName);
					var fileExt = fileName.split(".");
					if( fileExt.length === 1 || ( fileExt[0] === "" && fileExt.length === 2 ) ) {
							return "";
					}
					return fileExt.pop();
			}
			var getFileName = function(fileName){
					console.log("Costam1");
					console.log(fileName);
					var fileExt = fileName.split(".");
					return fileExt[0];
			}
			cb(null, getFileName(file.originalname) + "_"+Date.now() + '.' + getFileExt(file.originalname))
		}
});
var upload = multer({ storage : storage}).single('userPhoto');
//check if user is logged in
router.use(function(req,res,next){
console.log("Checking if logged");
if (req.session&&req.session.passport){
  res.locals.user = req.user;
  console.log(req.user.Name);
  res.locals.username = req.user.username;
  if(req.session.passport.user.Librarian){
     res.locals.librarian=true;
    console.log("Successful login as librarian");
  }
  else{
    res.locals.librarian=false;
    console.log("Successful login as reader");
  }
    
}
else{
  console.log("Fail")
}
next();
});
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Biblioteka',texttitle: 'Witaj na stronie Biblioteki Czytalskiej!',text : 'Na tej stronie możesz przejrzeć swoje wypożyczone książki oraz zbiory Biblioteki'});
});

router.get('/uploader',function(req,res,next){
  db.passport.authenticate('local', function(err, user) {
    if (req.session&&req.session.passport){
      if(req.session.passport.user.Librarian){
         res.render('uploader');
      }else{
        res.render('noauth', {logged : true});
      }
    }else{
      res.render('noauth', {logged : false});
    }
  })(req, res);
});

router.post('/api/photo',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            res.end("File is not uploaded");
        }
        res.end("File is uploaded");
    });
});
router.get('/login',function(req,res,next){
      res.render('login',{ title: 'Zalogowano'});
});
/*Handle Login POST */
router.post('/login', db.passport.authenticate('local', {
  successRedirect: '/login',
  failureRedirect: '/',
  failureFlash : true 
}));
router.post('/logout', function(req, res) 
{
    console.log("logging out ......");
    req.session.destroy();
    req.logout();
    res.redirect('/');
});
router.get('/search', function(req, res) {
  res.render('search',{ title: 'Wyszukaj książkę'});
});
router.get('/addbook', function(req, res) {
  db.passport.authenticate('local', function(err, user) {
    if (req.session&&req.session.passport){
      if(req.session.passport.user.Librarian){
        res.render('addbook',{ title: 'Dodaj książkę'});
      }else{
        res.render('noauth', {logged : true});
      }
    }else{
      res.render('noauth', {logged : false});
    }
  })(req, res);
});
router.post('/addbook', function (req, res) {
  db.Book
  .build({ISBN: req.body.ISBN,Name: req.body.Name, AuthorName: req.body.AuthorName,Year: req.body.Year,Description: req.body.Description})
  .save()
  .then(function(anotherTask) {
      res.render('index', { title: 'Sukces',texttitle: 'Sukces!',text : 'Książka dodana do zbiorów'});
  }).catch(function(error) {
      res.render('index', { title: 'Błąd',texttitle: 'Błąd!',text : 'Błąd przy dodawaniu książki'});
  })
});
router.get('/book', function(req, res) {
  console.log(req.query.ISBN);
  db.Book.findOne({
    where: { ISBN:req.query.ISBN }}).then(function(book) {
    res.render('book', {
      title: book.Title,
      book: book
    });
  });
});
router.get('/books',(req,res)=>{
  console.log( req.query.name);
  console.log( req.query.author);
  db.Book.findAll({
    where: { Title: {$like: '%'+req.query.name+'%'} , AuthorName: {$like: '%'+req.query.author+'%'}}}).then(function(books) {
    res.render('books', {
      title: 'Znalezione książki',
      books: books
    });
  });
});
router.get('/mybooks',(req,res)=>{
  db.passport.authenticate('local', function(err, user) {
    if (req.session&&req.session.passport){
      db.sequelize.query('select * from books,copies,users where books.ISBN=copies.BookISBN and users.Id=copies.UserId and users.Id='+req.session.passport.user.Id, { model: db.Book }).then(books => {
            res.render('mybooks', {
            title: 'Wypożyczone książki',
            books: books
          });
        })
        
      }
    else{
      res.render('noauth', {logged : false});
    }
  })(req, res);
});
module.exports = router;
