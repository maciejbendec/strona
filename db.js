//////////////SEQUELIZE
var exports = module.exports = {};
var Sequelize = require('sequelize');
var DataTypes = require('sequelize/lib/data-types');
 var sequelize = new Sequelize('baza','', '',
  {
   dialect: 'sqlite',
   storage: '/home/maciej/hello/baza.db',
  }
 );
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('users', 
  {
    Id : DataTypes.INTEGER,
    Name: DataTypes.STRING,
    Surname : DataTypes.STRING,
    Librarian : DataTypes.BOOLEAN,
    Login : DataTypes.STRING,
    Password : DataTypes.STRING
  },{
      timestamps: false
  });

  return User;
};

User.sync();
//////////////END SEQUELIZE
//////////////////////////PASSPORT
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
// Serialize sessions
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});
// Use local strategy to create user account
passport.use(new LocalStrategy(
  function(Login, Password, done) {
    User.find({ where: { Login: Login }}).then(function(user) {
      if (!user) {
        done(null, false, { message: 'Unknown user' });
      } else if (Password != user.Password) {
        done(null, false, { message: 'Invalid password'});
      } else {
        done(null, user);
      }
    }).error(function(err){
      done(err);
    });
  }
));
////////////////////// END PASSPORT
module.exports=passport;