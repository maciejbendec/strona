/*User*/
drop table if exists users;
create table users (
Id int primary key,
Name varchar(20),
Surname varchar(30),
Librarian int,
Login varchar(20),
Password varchar(20));

/*Book*/
drop table if exists books;
create table books(
ISBN varchar(20) primary key,
Title varchar(50),
AuthorName varchar(50),
Year int,
Description varchar(1000));
/*Copy*/
drop table if exists copies;
create table copies(
Id int primary key,
BookISBN varchar(20),
UserId int);


/*User dump*/
insert into users values (0,'Maciej','Bendec',1,'login','password');
insert into users values (1,'Czytelnik','Czytalski',0,'sezamie','otwieraj');
/*Author dump*/
insert into books values ('978-83-7659-900-7','Ojciec Chrzestny','Mario Puzo',1969,'Nic nie mogę napisać, omerta...');
insert into books values ('978-83-8097-708-2','Carrie','Stephen King',1974,'Bo to zła kobieta była!'); 
/*Copy dump*/
insert into copies values (0,'978-83-7659-900-7',0);
insert into copies values (1,'978-83-7659-900-7',1);
insert into copies values (2,'978-83-7659-900-7',NULL);
insert into copies values (3,'978-83-8097-708-2',0);
